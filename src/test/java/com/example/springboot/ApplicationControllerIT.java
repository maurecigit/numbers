package com.example.springboot;

import static org.assertj.core.api.Assertions.*;

import java.net.URL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationControllerIT {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

    @BeforeEach
    public void setUp() throws Exception {
    	
        this.base = new URL("http://localhost:" + port + "/");
        
    }

    /**
     * Minimum webservice test
     * 
     * @throws Exception
     */
    @Test
    public void getMinimum() throws Exception {
    	
        ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
        assertThat(response.getBody().equals("Working web service!"));
        
    }
    
    /**
     * Minimum webservice test with number
     * 
     * @throws Exception
     */
    @Test
    public void getNumber() throws Exception {
    	
        ResponseEntity<String> response = template.getForEntity(base.toString().concat("1042"), String.class);
        assertThat(response.getBody().equals("mil e quarenta e dois"));
        
    }
}
