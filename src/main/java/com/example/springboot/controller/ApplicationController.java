package com.example.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.service.ApplicationService;

@RestController
public class ApplicationController {
	
	@Autowired
	private ApplicationService appService;
	
	@RequestMapping("/")
	public String index() {
		return "Working web service!";
	}
	
	/**
	 * Return converse number to text
	 * 
	 * @return String
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{numbers}")
	public String findExtensive(@PathVariable int numbers) {
		
		//return "aaaGreetinBoot!aaa";
		return appService.findExtensive(numbers);
		
	}

}
