package com.example.springboot.service;

import org.springframework.stereotype.Service;

@Service
public class ApplicationService {
	
	/**
	 * Return the number in format text.
	 * 
	 * @return text
	 */
	public String findExtensive(int numbers) {
		
		return extensive(numbers);
	}
	
	/**
	 * Return the number in format text.
	 * 
	 * @param numbers
	 * @return String
	 */
	public static String extensive(int numbers) {
		
		String extensive = "";
		
		// hundreds
		String hundred = (numbers % 100) == 0 ? "cem" : "cento";
		String[]  hundreds = new String[]{"zero", hundred, "duzentos", "trezentos", "quatrocentos", "quinhentos",
			            "seiscentos", "setecentos", "oitocentos", "novecentos"};
		
		// Numerous basics
		String[]  basicNumbers = new String[]{"zero", "um", "dois", "três", "quatro", "cinco",
            "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze",
            "dezesseis", "dezessete", "dezoito", "dezenove"};
		
		// Dozens 		
		String[]  dozens = new String[]{"zero", "dez", "vinte", "trinta", "quarenta", "cinquenta",
            "sessenta", "setenta", "oitenta", "noventa"};
        
		// Case zero
        if (numbers == 0) {
        	
            return "zero";           
        }
        
        // Case is out of valid range
        if (numbers > 99999 || numbers < -99999) {
        	
        	return "Erro: Número fora do intervalo [-99999,99999]";
            
        }

        // Signal negative
        if (numbers < 0) {
        	
            extensive = "menos ";
            numbers *= -1;
        }

        // Indices of arrays
        int dozenId = (numbers % 100) / 10;
        int basicNumbersId = (numbers % 10);
        basicNumbersId += dozenId == 1 ? 10 : 0;	// include numbers from 10 to 19
        int hundredId = (numbers % 1000)/100;
        int thousandId = (numbers / 1000);

        // Formatter miler
        if (thousandId >= 20) {   // Above 19999
        
            extensive += dozens[thousandId / 10];

            if (thousandId % 10 != 0) {   // Dozen not ending with 0, you must write the unit
            	
                extensive += " e " + basicNumbers[thousandId % 10];
            }

            extensive += " mil";
            
        } else {
        	
            if (thousandId % 20 > 1) {    // if Above 1999
            	
                extensive += basicNumbers[thousandId] + " ";
            }

            if (thousandId % 20 != 0) {   // if Above 999
             
            	extensive += "mil";
            }
        }

        if (hundredId > 0) {  // if Above 99
           
        	extensive += ConcatenaE(extensive) + hundreds[hundredId];
        }

        if (dozenId > 1) {   // if Above 19
           
        	extensive += ConcatenaE(extensive) + dozens[dozenId];
        }

        if (basicNumbersId > 0) {   // If it is not zero
         
        	extensive += ConcatenaE(extensive) + basicNumbers[basicNumbersId];
        }
		
		return extensive;
	}
	
	/**
	 * Return " e " 
	 * 
	 * @param extensive
	 * @return String
	 */
	private static String ConcatenaE(String extensive)
    {

        return ((extensive != "" && extensive != "menos " ? " e " : ""));
    }

}
