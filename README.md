## Executando a aplicação

```
O Que Você Vai Precisar

http://www.oracle.com/technetwork/java/javase/downloads/index.html [JDK 8] ou posterior.


1. Para rodar a aplicação: 
	Navegue até a pasta inicial ( onde contêm no mínimo 10 arquivos ).
	Botão direito do mouse numa área vazia dentro da pasta e selecione "Git Bash Here".
	Cole o comando sem as aspas "./gradlew bootRun" e tecle "enter".
	
	Abra um navegador e vá até "http://localhost:8080/10003"
	
	Ajuda: https://github.com/spring-guides/gs-spring-boot ou https://spring.io/guides/gs/spring-boot/
	no tópico "Run the Application".
```

## Obtendo o projeto do github

	
	Página do bitbucket: https://bitbucket.org/maurecigit/numbers/src/develop/
	
    $ mkdir -p /workspace/app-server
    $ cd /workspace/app-server
    $ git clone https://maurecigit@bitbucket.org/maurecigit/numbers.git

    Ou dowload: https://bitbucket.org/maurecigit/numbers/get/develop.zip


## Pilha de tecnologias
```
	* Spring Boot
	* Java 8
	* Gradle
	* Sem Banco de dados
	* Este é o server back-end.
```


## Executando a aplicação no eclipse

```
O Que Você Vai Precisar

Na IDE eclipse Import "Existing Maven Projects".


1. Para rodar a aplicação no eclipse [botão direito: Run As/Java Application] em cima do arquivo
"/spring-boot/src/main/java/com/example/springboot/Application.java".
```


## Entendo o Código Fonte

```
São apenas quatro arquivos:

/src/main/java/com/example/springboot/controller/ApplicationController.java
/src/main/java/com/example/springboot/service/ApplicationService.java

/src/test/java/com/example/springboot/ApplicationControllerIT.java
/src/test/java/com/example/springboot/ApplicationControllerTest.java

Os demais arquivos são default dos aplicativos "Spring Boot" + ( Gradle ou Maven ).
```


## API's disponíveis



Operações para retornar o extenso do numero inteiro enviado no path

```
HTTP : GET	-	Path : http://localhost:8080/10003

	# Retorno: dez mil e três	
```


Obs.: Não fiz um ambiente Docker porque o computador aqui não habilita a virtualização.